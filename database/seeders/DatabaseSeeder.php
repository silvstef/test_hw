<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $admin = new \App\Models\User();
        $admin->name = 'Admin';
        $admin->surname = 'Kakiburu';
        $admin->username = 'Admin';
        $admin->password = bcrypt('Admin');
        $admin->nationality = 'Malta';
        $admin->is_admin = 1;
        $admin->save();
        \App\Models\User::factory(100)->create()->each(function($user) {
            $user->volumes()->saveMany(\App\Models\Volume::factory((int)rand(1,5))->make());
        });
    }
}
