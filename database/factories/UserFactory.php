<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name_surname = $this->faker->name;
        $tmp = explode(" ",$name_surname);
        $name = $tmp[0];
        $surname = isset($tmp[1]) ? $tmp[1] : 'Smith';
        return [
            'name' => $name,
            'surname' => $surname,
            'username' => strtolower(str_replace(" ", "_", $name_surname)),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'nationality' => $this->faker->country,
        ];
    }
}
