<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function list()
    {
        return fractal(User::with('volumes')->get(), new UserTranformer())->respond();
    }
}
