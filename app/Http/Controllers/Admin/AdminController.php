<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Volume;
use League\Fractal;

class AdminController extends Controller
{
    public function createUser(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'surname' => 'required|string',
            'username' => 'required',
            'nationality' => 'required|string',
            'password' => 'string|min:8'
        ]);
        $password = $request->password;
        if($password == '')
        {
            $password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi'; // password 
        }

        $user = new User();
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->username = $request->username;
        $user->nationality = $request->nationality;
        $user->password = bcrypt($password);
        if($user->save())
        {
            return fractal($user, new UserTranformer())->respond();
        }
        return fractal($user, new UserTranformer())->respond(507);
    }

    public function editUser(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'surname' => 'required|string',
            'nationality' => 'required|string',
            'username' => 'required',
        ]);

        $user = User::find($request->id);

        abort_if($user === null, 404, 'User not found');
        
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->username = $request->username;
        $user->nationality = $request->nationality;
        if($user->save())
        {
            return fractal($user, new UserTranformer())->respond();
        }
        return fractal($user, new UserTranformer())->respond(507);
    }

    public function deleteUser(Request $request)
    {
        $user = User::find($request->id);
        abort_if($user === null, 404);

        if($user->delete())
        {
            return fractal($user, new UserTranformer())->respond();
        }
        return fractal($user, new UserTranformer())->respond(507);
    }

    public function createVolume(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|max:190',
            'published_at' => 'required|date',
            'isbn' => 'required',
            'user_id' => 'required',
        ]);
        
        $user = User::find($request->user_id);
        abort_if($user === null, 404, 'User not found.');
        $volume = new Volume();
        $volume->title = $request->title;
        $volume->published_at = $request->published_at;
        $volume->isbn = $request->isbn;
        $volume->user_id = $request->user_id;

        if($volume->save())
        {
            return fractal($volume, new VolumeTransform())->respond();
        }
        return fractal($volume, new VolumeTransform())->respond(507);
    }

    public function editVolume(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'title' => 'required|string|max:190',
            'published_at' => 'required|date',
            'isbn' => 'required',
            'user_id' => 'required',
        ]);

        $volume = Volume::find($request->id);
        abort_if($volume === null, 404, 'Volume not found.');
        
        $user = User::find($request->user_id);
        abort_if($user === null, 404, 'User not found.');
        $volume->title = $request->title;
        $volume->published_at = $request->published_at;
        $volume->isbn = $request->isbn;
        $volume->user_id = $request->user_id;

        if($volume->save())
        {
            return fractal($volume, new VolumeTransform())->respond();
        }
        return fractal($volume, new VolumeTransform())->respond(507);
    }

    public function deleteVolume(Request $request)
    {
        $volume = Volume::find($request->id);
        abort_if($volume === null, 404);

        if($volume->delete())
        {
            return fractal($volume, new VolumeTranformer())->respond();
        }
        return fractal($volume, new VolumeTranformer())->respond(507);
    }
}
