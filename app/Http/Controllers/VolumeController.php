<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VolumeController extends Controller
{
    public function list()
    {
        return fractal(Volumes::with('user')->get(), new VolumeTranformer())->respond();
    }
}
