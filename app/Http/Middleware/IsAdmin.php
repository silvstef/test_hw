<?php

namespace App\Http\Middleware;

use Closure;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        if($user === null)
        {
            return middlewareError();
        }
        
        if ($user->is_admin) 
        {
            return $next($request);
        }
        return middlewareError();

    }
}
