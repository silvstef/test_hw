<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VolumeController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => 'auth:api'], function () {
    Route::group(['middleware' => 'isAdmin', 'namespace' => 'Admin'], function () {
        Route::post('user/create', [AdminController::class, 'createUser']);
        Route::post('user/edit', [AdminController::class, 'editUser']);
        Route::delete('user', [AdminController::class, 'deleteUser']);
        Route::post('volume/create', [AdminController::class, 'createVolume']);
        Route::post('volume/edit', [AdminController::class, 'editVolume']);
        Route::delete('volume', [AdminController::class, 'deleteVolume']);
    });
    Route::get('authors', [UserController::class, 'list']);
    Route::get('volumes', [VolumeController::class, 'list']);
});
